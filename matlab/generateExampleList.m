function generateExampleList()
    [pth, ~, ~] = fileparts(mfilename('fullpath'));
    exfldr = fullfile(pth, '..', 'examples');
    if ~exist(exfldr, 'dir')
        mkdir(exfldr)
    end
    outfile = fullfile(exfldr, 'example_list.txt');
    [f, msg] = fopen(outfile, 'wt');
    if f == -1
        error(msg)
    end
%     modules = mrstPath();
    modules = getCanonicalModulesMRST()
    modules{end+1} = 'core';
    for modNo = 1:numel(modules)
        module = modules{modNo};
        
        examples = mrstExamples(module);
        % Only one module, always picks first
        examples = examples{1};
        for i = 1:numel(examples)
            ex = java.io.File(examples{i});
            examples{i} = ex.getCanonicalPath();
        end
        fprintf(f, '%s\n', examples{:});
    end
    
    
    fclose(f);
end
