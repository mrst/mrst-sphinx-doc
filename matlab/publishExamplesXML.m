function publishExamplesXML(varargin)
    mrstVerbose off
    if mod(nargin, 2) == 1
        modules = varargin{1};
        if ischar(modules)
            modules = {modules};
        end
        varargin = varargin(2:end);
    else
        modules = {'core'};
    end
    opt = struct('replace', false, 'extension', 'xml', 'catchError',false);
    opt = merge_options(opt, varargin{:});
    
    count = 0;
    total = 0;
    for modNo = 1:numel(modules)
        module = modules{modNo};
        
        diary(module)
        diary on
        
        examples = mrstExamples(module);
        examples = examples{1};
        for exNo = 1:numel(examples)
            example = examples{exNo};
            if strcmpi(module, 'core')
                mrstModule reset;
            else
                mrstModule('reset', module);
            end
            isOk = isExamplePublished(example, opt.extension);
            if ~isOk || opt.replace
                fprintf('Publishing example %s...', example);
                close all;
                gravity reset off;
                pstatus = pause('query');
                pause('off')
                try
                    run_publish(example, struct('format', opt.extension, 'maxOutputLines', 8, 'catchError', opt.catchError))
                    fprintf(' Done.\n');
                    count = count + 1;
                catch e
                    fprintf('\n *** Error in publish example: %s\n', e.message);
                end
                pause(pstatus);
                close all; 
            else
                fprintf('Example %s already published, skipping...\n', example);
            end
            total = total + 1;
        end
        diary off
    end
    fprintf('All done! Published %d of %d total examples\n', count, total);
end

function [pth, name, ext] = getPublishedPath(example, extension)
    tmp = which(example);
    [expth, filename, ext] = fileparts(tmp);
    
    pth = fullfile(expth, 'html');
    name = filename;
end

function isPublished = isExamplePublished(example, ext)
    [pth, name, ~] = getPublishedPath(example, ext);
    
    isPublished = exist(fullfile(pth, [name, '.', ext]), 'file') > 0;
end

function run_publish(example, opt)
    publish(example, opt);
end