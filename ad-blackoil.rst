`ad-blackoil`: Black-oil solvers
*******************************************

.. include:: mrst/ad_blackoil/README.txt

Models
===============================

Base classes
-------------------------------
.. automodule:: ad_blackoil.models
	:members:
	:show-inheritance:

Utilities
===============================
.. automodule:: ad_blackoil.utils
	:members:

Examples
===============================
.. include:: examples/examples_ad_blackoil.txt
