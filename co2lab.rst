`co2lab`: Numerical CO\ :sub:`2` laboratory
*******************************************

.. include:: mrst/co2lab/README

.. automodule:: co2lab.interactive_tools
	:members:

.. automodule:: co2lab.interactive_tools.helpers
	:members:

.. automodule:: co2lab.interactive_tools.utils
	:members:


.. automodule:: co2lab.solvers
	:members:

.. automodule:: co2lab.solvers.equations
	:members:

.. automodule:: co2lab.solvers.equations.utils
	:members:

.. automodule:: co2lab.solvers.fluids
	:members:

.. automodule:: co2lab.solvers.models
	:members:

.. automodule:: co2lab.solvers.ve_incomp
	:members:

.. automodule:: co2lab.solvers.ve_incomp.h_formulation
	:members:

.. automodule:: co2lab.solvers.ve_incomp.s_formulation
	:members:

.. automodule:: co2lab.solvers.ve_incomp.params
	:members:

.. automodule:: co2lab.solvers.ve_incomp.VEmex
	:members:

.. automodule:: co2lab.utils
	:members:

.. automodule:: co2lab.utils.atlasgrid
	:members:

.. automodule:: co2lab.utils.co2props
	:members:

.. automodule:: co2lab.utils.grid
	:members:

.. automodule:: co2lab.utils.mappings
	:members:

.. automodule:: co2lab.utils.misc
	:members:

.. automodule:: co2lab.utils.optimization
	:members:

.. automodule:: co2lab.utils.plotting
	:members:

.. automodule:: co2lab.utils.trapping
	:members:

.. automodule:: co2lab.utils.trapping.cell_based
	:members:

.. automodule:: co2lab.utils.trapping.node_based
	:members:

.. automodule:: co2lab.utils.wells
	:members:


Examples
===============================
.. include:: examples/examples_co2lab.txt
