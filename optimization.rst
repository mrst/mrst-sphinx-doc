`optimization`
*******************************************
.. include:: mrst/optimization/README.txt


Objective functions
==============================

.. automodule:: optimization.objectives
	:members:

Utilities
==============================

Optimization
--------------------------
.. automodule:: optimization.optim
	:members:

.. automodule:: optimization.utils
	:members:


Examples
===============================
.. include:: examples/examples_optimization.txt
