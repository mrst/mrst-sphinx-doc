`ad-props`: PVT, fluid models and other properties for the AD-solvers
*******************************************

.. include:: mrst/ad_props/README.txt

Props
===============================
.. automodule:: ad_props
	:members:

.. automodule:: ad_props.props
	:members:

.. automodule:: ad_props.simple
	:members:

Utilities
===============================
.. automodule:: ad_props.utils
	:members:


Examples
===============================
.. include:: examples/examples_ad_props.txt
