`heterogeneity`
*******************************************

.. automodule:: heterogeneity.utils
	:members:

Examples
===============================
.. include:: examples/examples_heterogeneity.txt
