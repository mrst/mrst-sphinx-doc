`fvbiot`: Consistent finite-volume discretizations for poroelasticity
*******************************************
.. include:: mrst/fvbiot/README.md

.. automodule:: fvbiot
	:members:

Examples
===============================
.. include:: examples/examples_fvbiot.txt
