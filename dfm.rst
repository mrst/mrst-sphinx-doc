`dfm`: Discrete fracture matrix implementation
*******************************************
.. include:: mrst/dfm/README

.. automodule:: dfm
	:members:

.. automodule:: dfm.plotting
	:members:

.. automodule:: dfm.msfv_dfm
	:members:

.. automodule:: dfm.msfv_dfm.util
	:members:

Examples
===============================
.. include:: examples/examples_dfm.txt
