`diagnostics`: Flow diagnostics functionality
*******************************************
.. include:: mrst/diagnostics//README

.. automodule:: diagnostics.utils
	:members:

.. automodule:: diagnostics.utils.optimization
	:members:

Examples
===============================
.. include:: examples/examples_diagnostics.txt
