`nwm`: Near Wellbore Modelling
*******************************************

.. automodule:: nwm.models
	:members:

.. automodule:: nwm.gridding
	:members:
	
.. automodule:: nwm.trans
	:members:
	
.. automodule:: nwm.utils
	:members:	
	
	
Examples
===============================
.. include:: examples/examples_nwm.txt
