`adjoint`: Two-phase, incompressible adjoint solvers
*******************************************
.. include:: mrst/adjoint/README

Utilities
===============================
.. automodule:: adjoint
	:members:

.. automodule:: adjoint.objectives
	:members:

Examples
===============================
.. include:: examples/examples_adjoint.txt
