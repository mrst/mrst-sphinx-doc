`dg`: Discontinous Galerkin discretizations
*******************************************


.. automodule:: dg
	:members:

.. automodule:: dg.models
	:members:
	
.. automodule:: dg.models.utils
	:members:	

.. automodule:: dg.ad
	:members:

.. automodule:: dg.cubatures
	:members:

.. automodule:: dg.discretizations
	:members:

.. automodule:: dg.statefunctions
	:members:

.. automodule:: dg.statefunctions.flowprops
	:members:
	
.. automodule:: dg.statefunctions.flux
	:members:	
	
	
Examples
===============================
.. include:: examples/examples_dg.txt
