MRST Release notes
*******************************************

Release 2019b
===============================

Highlights
-------------------------------
The new release comes with a number of bug fixes, improvements and increased documentation coverage. We especially highlight:
<li>Several new modules have been added, including support for discontinuous Galerkin (dG) methods, geothermal simulation capability, near-wellbore modelling, nonlinear finite-volume schemes (NTPFA/NMPFA) and a vertex-approximate gradient scheme. Many of these modules are either fully or in-part contributed by external authors.
<li>Many new examples, some of which are early previews of content from the upcoming book on advanced modelling with MRST.
<li>The AD simulators have many improved features, including generalized sequential models and better support for CPR variants.


New modules and features
-------------------------------


<h3>Module dg</h3>
<ul>
<li>Implementation of Discontinuous Galerkin Methods (dG) for the transport subproblem in immiscible multiphase flow simulations
<li>Implements several useful features for element-type discretizations
	<ul>
	<li>DGDiscretization class: The core of the module, with implementation of inner products, logics for accessing degrees of freedom, etc.
	<li>Cubature class: Multiple cubature rules for unstructured elements
	<li>Polynomial class: Facilitates easy definition of polynomial basis functions, with operator implementation of tensor product, gradient, etc.
	<li>SpatialVector class: Supports spatial AD vectors for computation of gradients in cells
	</ul>
<li>Higher order than dG(1) is currently only supported for Cartesian grids
<li>The module is in an early development stage, and will be subject to potentially large changes in the near future
</ul>

<h3>Module geothermal
<ul>
<li>Module for geothermal simulations
<li>Supports single-phase, single-component water and single-phase, two-component water/NaCl models
<li>Implements the standard conservation equations for water and NaCl mass, in addition to an equation for conservation of energy
<li>Includes pressure/temperature/NaCl-dependent equations of state for density and viscosity
</ul>

<h3>Module nwm</h3>

<p>The nwm is a new module for the implementation and manipulation of near-wellbore modeling (NWM) methodology. The nwm provides several useful routines:
<ul>	
<li>Local grid reconstructions in the near-wellbore region originally modeled by the Corner-point grid (CPG) or Cartesian grid. The layered unstructured (Voronoi or triangular) grid in the near-wellbore region, namely volume of interest (VOI), glues the CPG to the layered radial grid built along the well trajectory. The hybrid NWM grid is assembled by the three sub grids.
<li>Mapping of the rock properties from original CPG to NWM grid.
<li>Hybrid transmissibility calculations for different sub grids (support the radial transmissibility).
<li>Generation of non-neighboring connections (NNC) to connect the sub grids.
<li>Automatic conversion of data structures given in ECLIPSE deck file, conforming to the original CPG, to the standard mrst data structures for the NWM hybrid.
<li>Support for multi-segment well (MSW) definition, and generation of node and segment information through grid-based methods.
<li>Several useful gridding tools, such as constructions of 2D Cartesian-radial hybrid grids, Cartesian-Voronoi hybrid grids and inclined layered grids.
</ul>

<h3>Module nfvm</h3>
<p>This module, developed by Wenjuan Zhang and Mohammed Al Kobaisi from the department of petroleum engineering in Khalifa University of Science and Technology, adds experimental support for nonlinear finite volume discretizations. It follows the development of paper W. Zhang, M. Al Kobaisi, Cell-centered nonlinear finite volume methods with improved robustness, SPE Journal (in print), 2019. 


<h3>Module vag</h3>
<ul>
<li>Implementation of a Vertex Approximate Gradient (VAG) scheme.
</ul>

<h2>Changes to MRST core</h2>
<ul>
<li>Several new datasets added from the Norwegian Petroleum Directorate
<li>New function mrstColorbar that improves the existing colorbarHist function
<li>New function merge_options_relaxed that trades the generality of merge_options for improved runtime performance.
</ul>

<h2>Changes to existing modules</h2>

<h3>Module ad-core</h3>
<ul>
<li>New example: differentRegionFunctionsExample demonstrates the use of the state function system to set different functions in different parts of the domain.
<li>New example: linearSolverExample demonstrating different linear solvers and different linear systems they may be used for (CPR, AMG, preconditioners and smoothers for fully-implicit, pressure and transport systems).
<li>simulatePackedProblem: Add support for specifying a restartStep, even when that time step is already simulated.
<li>Improved support for adaptive implicit (AIM) and explicit solvers.
<li>DiagonalAutoDiffBackend: Several performance improvements and bug fixes.
<li>NonLinearSolver now has separate parameters for both relaxation increment and decrement.
<li>getNonLinearSolver has been revised. The input options have changed somewhat.
<li>StateFunctions: Several improvements to getStateFunctionGroupingDependencyGraph and plots of graphs.
<li>New framework wells now has a rigorous implementation for cross-flow in both injectors and producers.
<li>plotWellSols: Better saving of plotted figures.
<li>CPRSolverAD: The relativeTolerance property now correctly applies to the subsolver. This fixes a discrepancy between the documentation and the implementation. The solver now uses the standard tolerance field to control the global solve. In addition, some logic has been added to ensure that the AMG solver improves the result at every iteration.
<li>PackedProblemManager: A class for managing a number of packed simulation problems, including some preliminary functionality for running ensambles on available cores in background threads.
<li>FacilityModel: Support for using no primary variables. Useful for strategies involving a mostly fixed well system.
<li>Function numelData mirrors numelData for ResultHandler and makes it easier to write code which can use either a cell array or a ResultHandler instance.
<li>selectLinearSolver now uses CPR by default (see linear solver release notes)
<li>LinearSolverAD now supports the “id” parameter together with a custom “disp” listing which makes it easier to see what parameters are set up.
<li>Linear solvers have generally been harmonized in what parameters are output in the report (to show convergence, iterations, etc)
<li>getMultiDimInterpolator is more accurate when computing derivatives.
<li>Fixed a bug in generic rel. perm. evaluator for non-three-phase models.
<li>Fix a scaling bug in well equations convergence check for generic models. It was previously too strict in many cases (missing division by surface densities).
</ul>

<h3>Module ad-props</h3>
<ul>
<li>Capillary pressure function handles are no longer assigned if the table is entirely made up of zero entries.
</ul>

<h3>Module ad-blackoil</h3>
<ul>	
<li>New examples showing variable implicitness (immiscibleTimeIntegrationExample, blackoilTimeIntegrationExample)
<li>Support for BC in generic solvers.
<li>Threshold pressures has been rewritten to subtract the threshold pressure from the gradient, instead of acting as an on/off-switch entirely.
<li>GenericBlackOilModel: Can now run with boundary conditions or source terms.
<li>Aquifer model based on Fetkovich aquifer model now including Eclipse input. 
</ul>
	
<h3>Module ad-eor</h3>
<ul>	
<li>Implementation of a blackoil model with surfactant and polymer.
</ul>

<h3>Module blackoil-sequential</h3>
<ul>	
<li>Support for generic pressure and transport models by calling PressureModel(model) and TransportModel(model) for some generic model. This will automatically generate a sequential scheme for that specific model, provided that the pressure reduction factors are known.
</ul>


<h3>Module compositional</h3>
<ul>
<li>standaloneFlash is less picky on dimensions for input arguments
<li>Several speed improvements in generic class of solvers
<li>Support for K-values with EOS for density predictions
<li>Rachford Rice bisection should be more robust.
<li>K-values are now initialized once, and stored per-cell. This means that the initial guess for flash is usually much better when transitioning from single-phase.
<li>Compositional properties now have a parameter useCompactEvaluation which avoids evaluating e.g. vapor properties in regions where vapor is not present.
</ul>
	
<h3>Module diagnostics</h3>
<li>Well pair region saturation values calculated in computeRTD.
<li>Option to compute RTDs in reverse direction of flow field.
<li>Explicit 1D saturation solver along reverse direction of backwards TOF. This provides improved flow diagnostics proxies when dealing with non-uniform initial reservoir saturations.


<h3>Module incomp</h3>
<ul>	
<li>Experimental support in the incompressible solvers for using AD fluids as inputs instead of the older implementation. The solvers will use the oil-water properties. Note that the fluids will still be treated as incompressible! The example “incompExampleWithADFluids” has been added.
</ul>


<h3>Module linearsolvers</h3>
<ul>	
<li>AMGCL solvers should be much easier to set up as many of the routines now document which options are available.
<li>Support for reuse of AMGCL solvers, for example to solve multiple right-hand sides.
<li>Add support to interface for block-CPR-solver added in AMGCL library.
<li>Support for storing partial preconditioners for CPR between solves.
<li>AMGCL required files (boost subset and specific commit from AMGCL repository) is now automatically fetched if not found. You will be prompted at first call to any AMGCL solver.
<li>Default AMG options have been improved for porous media-typical parameter ranges. Aggregation-based solvers are especially improved.
<li>Can now pass the number of threads as option.
</ul>


<h3>Module mrst-gui</h3>
<ul>	
<li>plotToolbar now has support for 1D grids.
</ul>
	
<h3>Module msrsb</h3>
<ul>
<li>getMultiscaleBasis: Updates to naming convention. The msrsb basis functions are no longer called rsb in type optional argument. If your script specifies basis functions, this will have to be revised.
<li>Many reworked examples
<li>Release of multiscale solvers for AD-type solvers
</ul>
 

<h3>Module upr</h3>
<ul>	
<li>Linearized computation of distance function in distmesh
<li>New default method for tessellation of curves (e.g. fault lines or well lines in 2D)
<ul>
	<li>Direct segmentation of each line segment to ensure exact conformity with input
	<li>Set keyword interpolWP = true (well lines) or interpolFL = true (fault lines) for previous behavior.
</ul>	
<li>Small adjustment of geometric tolerances, resulting in increased robustness
<ul>
	<li>Correct assignment of G.cells.tag and G.faces.tag also when distmesh removes duplicate points
</ul>
<li>Default PEBI gridding algorithm changed to clippedPebi2d in the functions pebiGrid and compositePebiGrid
<ul>
	<li>Set keyword useMrstPebi = true to use pebi.m from MRST-core
</ul>	
<li>Several new examples
</ul>





