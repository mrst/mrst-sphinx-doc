`compositional`: Equation-of-state compositional solvers
*******************************************

#.. include:: mrst/compositional/README.txt

Models
===============================

Base classes
-------------------------------
.. automodule:: compositional.models
	:members:
	:show-inheritance:

Different formulations
-------------------------------

.. automodule:: compositional.models.natvars
	:members:
	:show-inheritance:

.. automodule:: compositional.models.overall
	:members:
	:show-inheritance:

PVT models
===============================

Equations of state
--------------------
.. automodule:: compositional.models.eos
	:members:
	:show-inheritance:

Fluid models
--------------------

.. automodule:: compositional.fluids
	:members:
	:show-inheritance:

.. automodule:: compositional.fluids.blackoil
	:members:
	:show-inheritance:


Examples
===============================
.. include:: examples/examples_compositional.txt
