#!/bin/bash
ln -sfn ../../mrst-core core

ln -sfn ../../mrst-autodiff/ad-blackoil/ ad_blackoil
ln -sfn ../../mrst-autodiff/ad-core ad_core
ln -sfn ../../mrst-autodiff/ad-eor ad_eor

ln -sfn ../../mrst-autodiff/ad-mechanics ad_mechanics
ln -sfn ../../mrst-autodiff/ad-props ad_props
ln -sfn ../../mrst-solvers/adjoint adjoint
ln -sfn ../../mrst-multiscale/agglom agglom

ln -sfn ../../mrst-autodiff/blackoil-sequential blackoil_sequential
ln -sfn ../../mrst-book/book book

ln -sfn ../../mrst-co2lab/co2lab co2lab
ln -sfn ../../mrst-multiscale/coarsegrid coarsegrid
ln -sfn ../../mrst-autodiff/compositional compositional

ln -sfn ../../mrst-autodiff/compositional compositional
ln -sfn ../../mrst-autodiff/optimization optimization

ln -sfn ../../mrst-model-io/deckformat deckformat
ln -sfn ../../mrst-solvers/dfm dfm
ln -sfn ../../mrst-dg mrst_dg
ln -sfn ../../mrst-visualization/diagnostics diagnostics
ln -sfn ../../mrst-thirdparty-modules/dual-porosity dual_porosity

ln -sfn ../../mrst-thirdparty-modules/fvbiot fvbiot
ln -sfn ../../mrst-thirdparty-modules/geochemistry geochemistry
ln -sfn ../../mrst-thirdparty-modules/geothermal geothermal

ln -sfn ../../mrst-thirdparty-modules/heterogeneity heterogeneity
ln -sfn ../../mrst-solvers/hfm hfm

ln -sfn ../../mrst-solvers/incomp incomp

ln -sfn ../../mrst-model-io/libgeometry libgeometry
ln -sfn ../../mrst-solvers/linearsolvers linearsolvers

ln -sfn ../../mrst-core/utils/3rdparty/matlab_bgl matlab_bgl 
ln -sfn ../../mrst-model-io/mex mex 
ln -sfn ../../mrst-solvers/mimetic mimetic
ln -sfn ../../mrst-solvers/mpfa mpfa
ln -sfn ../../mrst-visualization/mrst-gui mrst_gui
ln -sfn ../../mrst-model-io/mrst_api mrst_api 
ln -sfn ../../mrst-multiscale/msfvm msfvm
ln -sfn ../../mrst-multiscale/msmfem msmfem
ln -sfn ../../mrst-multiscale/msrsb msrsb

ln -sfn ../../mrst-thirdparty-modules/nwm nwm

ln -sfn ../../mrst-model-io/opm_gridprocessing opm_gridprocessing
ln -sfn ../../mrst-autodiff/optimization optimization

ln -sfn ../../mrst-thirdparty-modules/re-mpfa re_mpfa

ln -sfn ../../mrst-autodiff/solvent solvent
ln -sfn ../../mrst-model-io/spe10 spe10
ln -sfn ../../mrst-multiscale/steady-state steady_state
ln -sfn ../../mrst-visualization/streamlines streamlines

ln -sfn ../../mrst-model-io/triangle triangle

ln -sfn ../../mrst-thirdparty-modules/upr upr
ln -sfn ../../mrst-multiscale/upscaling upscaling

ln -sfn ../../mrst-solvers/vag vag
ln -sfn ../../mrst-solvers/vem vem
ln -sfn ../../mrst-solvers/vemmech vemmech

ln -sfn ../../mrst-model-io/wellpaths wellpaths









