function x = singleOutputFunction(arg1, arg2, varargin)
% A version with a single output (short description)
%
% SYNOPSIS:
%   x = singleOutputFunction(arg1, arg2, 'Name', Value);
%
% DESCRIPTION:
%   Detailed description of function
%
% PARAMETERS:
%   arg1 - The first argument.
%
%   arg2 - The second argument. This argument has a longer documentation,
%          which is split over two lines.
%
% OPTIONAL PARAMETERS:
%
%  'Name'       - Description of the optional argument `Name`.
%
%  'SecondName' - Description of the optional argument `SecondName`.
%
% RETURNS:
%   returnType - A single return value with description.
%
% EXAMPLE:
%   % Description as comment
%   x = singleOutputFunction(arg1, arg2, 'Name', Value);
%
% SEE ALSO:
%   `multipleOutputFunction`, `plot`

%{
Copyright 2009-2017 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

end