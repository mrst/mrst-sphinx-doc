function simpleFunction(arg1, varargin)
% Minimal function (short description)
%
% SYNOPSIS:
%   x = simpleFunction(arg1);
%
% DESCRIPTION:
%   Detailed description of function
%
% PARAMETERS:
%   arg1 - The only argument.
%
% EXAMPLE: 
%   % Compute something
%   x = simpleFunction(arg1);
%
% SEE ALSO:
%   `multipleOutputFunction`, `singleOutputFunction`

%{
Copyright 2009-2017 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

end