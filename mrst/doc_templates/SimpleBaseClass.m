classdef SimpleBaseClass
    % A version with a single output (short description)
    %
    % SYNOPSIS:
    %   x = SimpleBaseClass(somearg, 'property', value);
    %
    % DESCRIPTION:
    %   This is the overall description of the class. Note the required
    %   indentation at the initial class documenation, and the copyright
    %   notice placed at the end of the document.
    %
    % PARAMETERS:
    %   arg1 - The first argument.
    %
    % OPTIONAL PARAMETERS:
    %  'propertyName'  - Any number of named properties. Corresponding
    %                    values are mapped onto class properties and
    %                    validated for type using `merge_options`.
    %
    % RETURNS:
    %  SimpleBaseClass - Initialized class instance
    %
    % EXAMPLE: 
    %   % Set up a class with random data:
    %   M = SimpleBaseClass(arg1, 'data', rand(10, 1));
    %
    % SEE ALSO:
    %   `multipleOutputFunction`, `plot`
    
properties
    property = 3 % Some very important property
    data = nan % This property may be less important, since it is not first.
    extra % Keep docstrings on one line for properties, or they break badly
end

methods
    function sbc = SimpleBaseClass(somearg, varargin)
        % Public constructor for class
        %
        % SYNOPSIS:
        %   x = SimpleBaseClass(arg1, varargin);
        %
        % DESCRIPTION:
        %   Detailed description of function
        %
        % PARAMETERS:
        %   arg1 - The only argument.
        %
        % RETURNS:
        %   v   - We return some value.
        %
        % SEE ALSO:
        %   `staticFunction`
        sbc = merge_options(opt, varargin{:});
    end
end

methods (Static)
    
    function v = staticFunction(arg1, arg2)
        % This is a static member function
        %
        % SYNOPSIS:
        %
        %   x = staticFunction(arg1);
        %
        % DESCRIPTION:
        %   Detailed description of function
        %
        % PARAMETERS:
        %   arg1 - The only argument.
        %
        % RETURNS:
        %   v   - We return some value.
        %
        % SEE ALSO:
        %   `SimpleBaseClass`

    end
end
end

%{
Copyright 2009-2017 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

