# Copyright 2009-2021 SINTEF Digital, Mathematics & Cybernetics.
# 
# This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).
# 
# MRST is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# MRST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with MRST.  If not, see <http://www.gnu.org/licenses/>.

import os
import xml.etree.ElementTree
import re
from os.path import *

def readPaths():
    exfile = open('examples/example_list.txt', 'r')
    # cpath = pwd();
    # cpath = os.path.dirname(os.path.realpath(__file__))
    ex = exfile.read()
    ex = ex.replace('mrst-core', 'core')
    ex = ex.replace('-', '_')
    return ex.splitlines()


def parse_publish_to_xml(exfile, relpath):
    tmp = os.path.split(exfile)
    example_filename = tmp[-1].replace('.xml','.m')
    f = open(exfile, 'r')
    example = f.read()
    f.close()
    example = example.replace("<tt>", "`")
    example = example.replace("</tt>", "`")
    #example = re.sub('\.\.\.\s+', '...' + os.linesep, example)
    #example = example.replace("...", "...\n")
    e = xml.etree.ElementTree.fromstring(example)
    # e = xml.etree.ElementTree.parse(exfile).getroot()
    # outfile  = open("D:/jobb/bitbucket/mrst-documentation/flowSolverTutorial1.txt", "w")
    output = ''

    def addIdent(txt):
        return "  ".join(("\n"+txt.lstrip()).splitlines(True)) + "\n"

    headcount = 0
    for el in e:
        if el.tag == "cell":
            # A cell which corresponds to a matlab section in cell mode
            for paragraphs in el:
                ptag = paragraphs.tag;
                if ptag == "steptitle":
                    # Heading of subsection
                    sect = "".join(paragraphs.itertext());
                    tit_length = len(sect);
                    if headcount == 0:
                        # Make heading
                        ch = '-';

                    else:
                        ch = '^'
                    sect = '\n' + sect + '\n' + ch * tit_length + '\n'
                    if headcount == 0:
                        sect = sect + '*Generated from ' + example_filename + '*\n\n'
                    headcount += 1;
                    output += sect
                elif ptag == "text":
                    # Simply text
                    for block in paragraphs:
                        if block.tag == "p":
                            # We found text
                            if block.text.strip():
                                #txt = block.text
                                #output += txt
                                #output += "\n"
                                output += "".join(block.itertext());
                            else:
                                # This paragraph is an equation
                                for subblock in block:
                                    if subblock.tag == "equation":
                                        output += "\n.. math::\n"
                                        eq = subblock.get("text")
                                        if eq:
                                            eq = eq.strip("$")
                                            eq = addIdent(eq)
                                            output += eq
                        output += "\n"
                elif ptag == "mcode" or ptag == "mcode-xmlized":
                    # Code block
                    for block in paragraphs:
                        #  print block
                        txt = "".join(block.itertext());
                        txt = re.sub('\.\.\.\s', '...' + os.linesep, txt)

                        crindex = txt.lower().find("copyright")
                        if crindex > -1:
                            # Strip copyright plus any comments etc before that.
                            txt = txt[0:crindex]
                            lastalpha = [m.end(0) for m in re.finditer('\w|\)|\;', txt)];
                            if lastalpha:
                               txt = txt[0:lastalpha[-1]]
                        output += "\n.. code-block:: matlab\n"
                        txt = addIdent(txt);
                        output += txt
                        output += "\n"
                elif ptag == "mcodeoutput":
                    # Another code block?? Slightly different format
                    output += "\n.. code-block:: none\n"
                    txt = addIdent(paragraphs.text);
                    output += txt
                    output += "\n"
                elif ptag == "img":
                    # Image. Use provided relative path.
                    img = relpath + "/" + paragraphs.get("src")
                    output += '.. figure:: ' + img + "\n"
                    output += "  :figwidth: 100%\n"
                    output += "\n"

    return output


files = readPaths()
# Get all possible modules
modules = [f for f in os.listdir('mrst') if isdir(normpath(join('mrst', f)))]
for module in modules:
    print(module)
    splitstr = os.sep + module + os.sep
    examples = filter(lambda x: splitstr in x, files)
    moduleText = ''
    for example in examples:
        if 'experimental' in example:
            print 'Skipping experimental example...'
            continue
        # Split path on first occurance of module name bracketed by the filesep
        ex = example.split(splitstr, 1)[1]
        [exfldr, exfile] = os.path.split(ex)
        [exname, exext] = os.path.splitext(exfile)
        xml_path = os.path.join('mrst', module, exfldr, 'html', exname + '.xml')
        relpath = "/".join(['mrst', module, exfldr, 'html'])
        if os.path.exists(xml_path):
            print "Found published example: " + xml_path
            published = parse_publish_to_xml(xml_path, relpath)
            moduleText += published + "\n"
        else:
            print "WARNING: Found unpublished example: " + xml_path
    if moduleText:
        outfile = open(os.path.join('examples', 'examples_' + module + '.txt'), 'w')
        outfile.write(moduleText.encode('utf-8'))
        outfile.close()
