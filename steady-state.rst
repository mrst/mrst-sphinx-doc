`steady-state` Steady-state upscaling of functions
*******************************************
.. include:: mrst/steady_state/README

.. automodule:: steady_state
	:members:

.. automodule:: steady_state.utils
	:members:

.. automodule:: steady_state.ad
	:members:

.. automodule:: steady_state.coarsegrid
	:members:

.. automodule:: steady_state.upscaling
	:members:

.. automodule:: steady_state.mex
	:members:

Examples
===============================
.. include:: examples/examples_steady_state.txt
