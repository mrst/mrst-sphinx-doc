`vemmech`: Mechanics for general grids using the virtual element method
*******************************************
.. include:: mrst/vemmech/README

.. automodule:: vemmech.utils
	:members:

.. automodule:: vemmech.grid_utils
	:members:

.. automodule:: vemmech.models
	:members:

.. automodule:: vemmech.plotting
	:members:

Examples
===============================
.. include:: examples/examples_vemmech.txt
