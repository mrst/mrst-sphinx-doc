`deckformat`: Reading and conversion of input decks
*******************************************

.. include:: mrst/deckformat/README

.. automodule:: deckformat
	:members:

.. automodule:: deckformat.deckinput
	:members:

.. automodule:: deckformat.grid
	:members:

.. automodule:: deckformat.params
	:members:

.. automodule:: deckformat.params.fluid
	:members:

.. automodule:: deckformat.params.rock
	:members:

.. automodule:: deckformat.params.wells_and_bc
	:members:

... automodule:: deckformat.resultinput
	:members:

Simulator
===============================
.. automodule:: deckformat.simulator
	:members:


Examples
===============================
.. include:: examples/examples_deckformat.txt
