`agglom`: Flow and property-based coarse-grid generation
*******************************************
.. include:: mrst/agglom/README

Utilities
===============================
.. automodule:: agglom
	:members:

.. automodule:: agglom.utils
	:members:


Examples
===============================
.. include:: examples/examples_agglom.txt
