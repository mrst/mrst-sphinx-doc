`ad-eor`: Enhanced oil recovery solvers
*******************************************

.. include:: mrst/ad_eor/README.txt

Models
===============================

Base classes
-------------------------------
.. automodule:: ad_eor.models
	:members:
	:show-inheritance:

Utilities
===============================
.. automodule:: ad_eor.utils
	:members:


Examples
===============================
.. include:: examples/examples_ad_eor.txt
