`book`: Book examples
*******************************************

.. include:: mrst/book/README

Examples
===============================
.. include:: examples/examples_book.txt
