
Basic VEM tutorial
------------------
*Generated from vemExample1.m*

In this example, we will introduce the virtual element method (VEM) solver in MRST, and show how to set up and use it to solve the single-phase pressure equation

.. math::

  \nabla\cdot v = q, \qquad v=\textbf{--}\frac{K}{\mu}\nabla p,
     \qquad x \in \Omega,

for a simple pressure drop problem.
The virtual element method constitutes a unified framework for higher-order methods on general polygonal and polyhedral grids. We combine the single-phase pressure equaiton into an ellpitic equation in the pressure only;

.. math::

  - \nabla \cdot K\nabla p = q.

Multiplying this by a function 
               
             and integrating over 
               
             yields the weak formulation: Find 
               
             such that

.. math::

  a(p, v) = \int_{\Omega}\nabla p \cdot K \nabla v d x
    = \int_\Omega q v d x, \qquad \forall v \in H^1_0(\Omega).

The bilinear form 
               
             can be split into a sum of local bilinear forms as

.. math::

  a(u,v) = \sum\nolimits_i
      \int_{\Omega_i} \nabla u \cdot K \nabla v d x
    = \sum\nolimits_i a^i(u,v),

where 
               
             are the cells of the grid. For a 
               
            -th order VEM, we construct approximate local bilinear forms 
               
             which are exact whevener one of 
               
             is a polynomial of degree 
               
             or less. When none of 
               
             are a such a polynomial, we only approximate 
               
             to the right order. To obtain this, we apply the following Pythagoras identity:

.. math::

  a^i(u,v) = a^i(\Pi u, \Pi v) + a^i(u-\Pi u, v- \Pi v),

where 
               
             is an 
               
            -orthogonal projection onto the space of polynomials of degree 
               
             or less. The approximate bilinear form is then

.. math::

  a_h^i(u,v) = a^i(\Pi u, \Pi v) + s^i(u-\Pi u, v- \Pi v),

where 
               
             is a stability term, which we are free to choose so long as the resulting bilinear form is positive definite and stable.  The flux field can easily be reconstructed from the pressure solution. For details on constructing the VEM function spaces, and implementing the method, see for example Klemetsdal 2016.

.. code-block:: matlab

  try
     require upr vem incomp vemmech
  catch
     mrstModule add upr vem incomp vemmech
  end


Define geometry
^^^^^^^^^^^^^^^
Since VEM is consistent for general polyhedral cells, we will use the UPR module to construct an arbitrary PEBI-grid. We consider a domain covering 
               
            . To generate the grid, will use the function mirroredPebi, in which we define the boundary of the domain, and the seed-points for the gridcells.

.. code-block:: matlab

  gridLimits = [500, 100, 100];
  
  boundary  = [0             0             0             ; ...
               gridLimits(1) 0             0             ; ...
               gridLimits(1) gridLimits(2) 0             ; ...
               0             gridLimits(2) 0             ; ...
               0             0             gridLimits(3) ; ...
               gridLimits(1) 0             gridLimits(3) ; ...
               gridLimits(1) gridLimits(2) gridLimits(3) ; ...
               0             gridLimits(2) gridLimits(3)];
  
  points = bsxfun(@times, rand(200, 3), gridLimits);
  
  G = mirroredPebi(points, boundary);

Having generated the grid structure, we plot the result.

.. code-block:: matlab

  clf, plotGrid(G); view(3); axis equal
  light('Position',[-50 -50 -100], 'Style', 'infinite')

.. figure:: mrst/vem/examples/html/vemExample1_01.png
  :figwidth: 100%

The VEM implementation uses grid properties that are not computed by the MRST-function computeGeometry, such as cell diameters and edge normals. Thus, we compute the geometry using computeVEMGeometry. Note that this function uses computeGeometry and the function createAugmentedGrid from the VEM module for geomechanics.

.. code-block:: matlab

  G = computeVEMGeometry(G);


Define rock and fluid properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
We set the permeability to be homogeneous and anisotropic,

.. math::

  K = \left(\begin{array}{ccc}
       1000 & 0  & 0 \\ 0 & 100 & 0 \\ 0 & 0 & 10 \end{array}\right). 

The rock structure is constructed usin makeRock, where we let the porosity be 0.3 in all cells. The fluid is constructed using initSingleFluid. Finally, we use initResSol to initialize the state.

.. code-block:: matlab

  perm = [1000, 100, 10].* milli*darcy();
  poro = 0.3;
  rock = makeRock(G, perm, poro);
  
  fluid = initSingleFluid('mu' , 100*centi*poise     , ...
                          'rho', 800*kilogram/meter^3);
  
  state = initResSol(G, 0);


Definig boundary conditions
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Since the PEBI-grid might be subject to roundoff-errors on the boundaries, we identify specific boundary faces by finding the ones that are within a given tolerance away from where we expect the boundary to be.

.. code-block:: matlab

  tol = 1e-6;
  
  bFaces = boundaryFaces(G);
  west   = abs(G.faces.centroids(bFaces,1)                ) < tol;
  east   = abs(G.faces.centroids(bFaces,1) - gridLimits(1)) < tol;

We define the western and easter boundaries to be Dirichlet boundaries with a prescribed pressure. Boundaries with no prescribed boundary condition are no-flow boundaries by default.

.. code-block:: matlab

  bc = addBC([], bFaces(west), 'pressure', 500*barsa);
  bc = addBC(bc, bFaces(east), 'pressure', 0        );


Constructing the linear systems
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
First, we compute the local inner products 
               
            . The implementation supports first and second order, and we will compute both. This is done using computeVirtualIP, where we must provide the grid and rock structures, and method order.

.. code-block:: matlab

  S1 = computeVirtualIP(G, rock, 1); disp(S1);
  S2 = computeVirtualIP(G, rock, 2);


.. code-block:: none

  Building with 'gcc'.
  Warning: You are using gcc version '7.4.0'. The version of gcc is not supported.
  The version currently supported with MEX is '6.3.x'. For a list of currently
  supported compilers see:
  https://www.mathworks.com/support/compilers/current_release. 
  MEX completed successfully.
               A: [4078×4078 double]
              ip: 'ip_simple'
  ...

The resulting solution structure has the following fields:

Next, we compute the solution using both first-order and second-order VEM. The first-order VEM uses the pressure at the nodes as degrees of freedom. However, it is possible to reconstruct the cell pressures of the approximated solution exactly, by uising the node pressures. This can be done by setting the optional input parameter 'cellPressure' to true in incompVEM. Moreover, we will invesitgate the linear system of the two methods later, so we set 'matrixOutput' to true.

.. code-block:: matlab

  state1 = incompVEM(state, G, S1, fluid, 'bc', bc, ...
                              'cellPressure', true, 'matrixOutput', true);
  state2 = incompVEM(state, G, S2, fluid, 'bc', bc, 'matrixOutput', true);

We then plot the results.

.. code-block:: matlab

  clf
  
  subplot(2,1,1)
  plotCellData(G, state1.pressure); axis equal off; colorbar; view(3);
  
  subplot(2,1,2)
  plotCellData(G, state2.pressure); axis equal off; colorbar; view(3);

.. figure:: mrst/vem/examples/html/vemExample1_02.png
  :figwidth: 100%


Degrees of freedom
^^^^^^^^^^^^^^^^^^
Finally, we investigate the linear systems obtained from the two methods. We start by identifying the cell closest to the middle of the domain, its node coordinates, and face and cell centroids.

.. code-block:: matlab

  distances = sum(bsxfun(@minus, G.cells.centroids, gridLimits/2).^2,2);
  midCell   = find(distances == min(distances));
  midCell   = midCell(1);
  
  n   = G.cells.nodes(G.cells.nodePos(midCell):G.cells.nodePos(midCell+1)-1);
  xn  = G.nodes.coords(n,:);
  f   = G.cells.faces(G.cells.facePos(midCell):G.cells.facePos(midCell+1)-1);
  xf  = G.faces.centroids(f,:);
  xc  = G.cells.centroids(midCell,:);

The first-order VEM uses the pressure at the nodes as degrees of freedom. The second-order VEM uses the pressure at the nodes and edge centroids, along with the average face pressure and average cell pressure. The latter yields a much denser system. We show this by indicating the degrees of freedom of the middle cell, along with a sparsity plot of the linear systems.

.. code-block:: matlab

  clf
  subplot(2,2,1)
  plotGrid(G, midCell, 'faceAlpha', 0.2); axis equal off;
  hold on
  plot3(xn(:,1), xn(:,2), xn(:,3), 'sq');
  
  subplot(2,2,2)
  spy(state1.A)
  
  subplot(2,2,3)
  plotGrid(G, midCell, 'faceAlpha', 0.2); axis equal off;
  hold on
  plot3(xn(:,1), xn(:,2), xn(:,3), 'sq')
  plot3(xf(:,1), xf(:,2), xf(:,3), 'd')
  plot3(xc(:,1), xc(:,2), xc(:,3), 'o')
  
  subplot(2,2,4)
  spy(state2.A)

.. figure:: mrst/vem/examples/html/vemExample1_03.png
  :figwidth: 100%

The construciton of these linear systems can be quite expensive in terms of computational effort, especially for reservoir models with many cells. In order to speed up the construction, one can use C-accelerated MEX functions in computeVirtualIP. This is done by setting 'invertBlocks' to 'MEX'. Note that this relies on being able to build the required MEX functions:

.. code-block:: matlab

  tic
  fprintf('Computing innerproducts using mldivide \t ...
  ');
  computeVirtualIP(G, rock, 2);
  toc
  
  tic
  fprintf('Computing innerproducts using MEX \t ...
  ');
  computeVirtualIP(G, rock, 2, 'invertBlocks', 'MEX');
  toc


.. code-block:: none

  Computing innerproducts using mldivide 	 ... Elapsed time is 1.073449 seconds.
  Computing innerproducts using MEX 	 ... Elapsed time is 0.792697 seconds.


For larger resrvoir models, using C-accelerated MEX functions is usually several orders of magnitude faster, and should always be used if possible.



Solving transport problems with VEM
-----------------------------------
*Generated from vemExample2.m*

In this example, we will use the virtual element method (VEM) solver in MRST, and show how to set up and use it to solve a transport problem in which we inject a high-viscosity fluid in a reservoir with varying permeability, which is initially filled with a low-viscosity fluid. To emphasize the importance of using a consistent discretization method for grids that are not K-orthogonal, we will compare the solution to the Two-point flux approximation (TPFA) mehod.

.. code-block:: matlab

  try
     require upr vem incomp vemmech
  catch
     mrstModule add upr vem incomp vemmech
  end


Define geometry
^^^^^^^^^^^^^^^
We will use the UPR module to construct a composite PEBI-grid covering the domain 
               
            , divided into three regions of highly varying permeability, with a source placed in the lower left corner, and a sink in the upper right corner. See the UPR module on how to contruct such grids.

.. code-block:: matlab

  [G, c] = producerInjectorGrid();

Having generated the grid structure, we plot the result.

.. code-block:: matlab

  clf, plotGrid(G, 'facecolor', 'none');
  srcCells = find(G.cells.tag);
  axis equal off

.. figure:: mrst/vem/examples/html/vemExample2_01.png
  :figwidth: 100%

The VEM implementation uses grid properties that are not computed by the MRST-function computeGeometry, such as cell diameters and edge normals. Thus, we compute the geometry using computeVEMGeometry. Note that this function uses computeGeometry.

.. code-block:: matlab

  G = computeVEMGeometry(G);


Define rock and fluid properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Numbering the grid regions from the lower left corner to the upper right corner, we set permeability in region 
               
             to be

.. math::

  K_i = R(\theta_i)\left(\begin{array}{cc}
       1000 & 0 \\ 0 & 10
       \end{array}\right)R(\theta_i)^T, 

in units of 100 mD, where 
               
             is a rotation matrix

.. math::

  R(\theta) = \left(\begin{array}{cc}
       \cos(\theta) & -\sin(\theta) \\ \sin(\theta) & \cos(\theta)
       \end{array}\right). 


.. code-block:: matlab

  K = diag([1000, 10])*milli*darcy();
  R = @(t) [cos(t) -sin(t); sin(t) cos(t)];
  
  KK{1} = R(pi/4)*K*R(pi/4)';
  KK{2} = K;
  KK{3} = R(pi/2)*K*R(pi/2)';
  KK{4} = K;

The structure c consists of four logical vectors identifying the cells belonging to each region, and we use this to set the permeability in each region. To define a full, symmetric permeability tensor, we specify the upper-triangular part. The rock structure is then constructed using makeRock, where we set the porosity to 0.3:

.. code-block:: matlab

  perm = zeros(G.cells.num, 3);
  for i = 1:numel(c)
      perm(c{i},:) = repmat(KK{i}([1,2,4]), nnz(c{i}), 1);
  end
  
  poro = 0.3;
  rock = makeRock(G, perm, poro);

We will consider a scenario in which we inject a high-viscosity fluid at a constant rate in the reservoir, which is initially filled with a fluid with lower viscosity. We define a fluid structure with these two fluids using initSimpleFluid. For simplicity, we set both phase relative permeability exponents to 1. Finally, we use initResSol to initialize the state. Here, we must indicate that the initial saturation is 0 for the fluid we are injecting, and 1 for the fluid which initially occupies the pore volume.

.. code-block:: matlab

  fluid = initSimpleFluid('mu' , [   5,    1]*centi*poise     , ...
                          'rho', [1000,  800]*kilogram/meter^3, ...
                          'n'  , [   1,    1]                      );
  
  [stateVEM, stateTPFA] = deal(initResSol(G, 0, [0,1]));


Definig source terms
^^^^^^^^^^^^^^^^^^^^
We set the injection rate equal one pore volume per 10 years. The source and sink terms are constructed using addSource. We must specify the saturation in the source term. Since want to inject fluid 1, we set this to [1,0]. We must also specify a saturation for the sink term, but this does not affect the solution.

.. code-block:: matlab

  Q   = sum(poreVolume(G,rock))/(10*year);
  src = addSource([], find(G.cells.tag), [Q, -Q], 'sat', [1,0; 0,1]);


Constructing the linear systems
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Next, we compute the two-point transmissibilties and virtual inner products. In this example, we use a first-order VEM.

.. code-block:: matlab

  STPFA = computeTrans(G, rock);
  SVEM  = computeVirtualIP(G, rock, 1);


.. code-block:: none

  Warning: Matrix is close to singular or badly scaled. Results may be inaccurate.
  RCOND =  1.847688e-17. 



Transport
^^^^^^^^^
We now solve the transport problem over a time period equal to the time it takes to inject one pore volume. While TPFA is locally conservative by construction, VEM is not, which may lead to unphysical saturations when the solution is applied to the transport solver. To avoid this, we must thus postprocess the VEM solution so that the fluxes are locally conservative. This is done using conserveFlux, in which we provide any sources, sinks and boundary conditions. As for the solvers in MRST, all boundaries without prescribed boundary conditions are interpreted as no-flow boundaries.
For each time step, we plot the saturation profiles and the produciton rate in the sink cell.

.. code-block:: matlab

  T  = 10*year;
  nT = 100;
  dT = T/nT;
  
  [productionRateVEM, productionRateTPFA] = deal(zeros(nT,1));
  
  t = linspace(0,T/year,nT);
  
  for i = 1:nT
  
      %   Solve TPFA pressure equation.
      stateTPFA = incompTPFA(stateTPFA, G, STPFA, fluid, 'src', src);
  
      %   Solve TPFA transport equation.
      stateTPFA = implicitTransport(stateTPFA, G, dT, rock, fluid, 'src', src);
  
      %   Plot results.
      subplot(2,2,1)
      plotCellData(G, stateTPFA.s(:,1), 'edgecolor', 'none')
      caxis([0,1]);
      title('TPFA');
      axis equal off
  
      %   Solve VEM pressure equation.
      stateVEM = incompVEM(stateVEM, G, SVEM, fluid, 'src', src, 'cellPressure', true);
  
      %   Postprocess solution.
      stateVEM = conserveFlux(stateVEM, G, rock, 'src', src);
  
      %   Solve VEM transport equation.
      stateVEM = implicitTransport(stateVEM, G, dT, rock, fluid, 'src', src);
  
      %   Plot the results.
      subplot(2,2,2)
      plotCellData(G, stateVEM.s(:,1), 'edgecolor', 'none')
      caxis([0,1]);
      title('VEM');
      axis equal off
  
      %   Plot production in producer.
      productionRateVEM(i)  = day*Q*stateTPFA.s(src.cell(2),2);
      productionRateTPFA(i) = day*Q*stateVEM.s(src.cell(2),2);
  
      subplot(2,2,3:4)
      h = plot(t(1:i), productionRateVEM( 1:i), ...
               t(1:i), productionRateTPFA(1:i), 'lineWidth', 2);
      axis([0 T/year 0 1.1*day*Q])
      xlabel('Time [years]');
      ylabel('Production rate [m^3/day]');
      legend('TPFA', 'VEM')
      drawnow
  
  end

.. figure:: mrst/vem/examples/html/vemExample2_02.png
  :figwidth: 100%

We see that there are significant differences in the two saturation profiles and production curves. This is due to the fact that TPFA fails to capture the effect of the rotated permeability fields in regions one and four.


