
Single-phase depletion of a dual-porosity reservoir. Fracture fluid is
----------------------------------------------------------------------
*Generated from example_1ph.m*

produced before the matrix. Note that the pressure drop is faster in the fractures, while the matrix drains slowly.

.. code-block:: matlab

  clear;
  clc;
  close all;
  
  mrstModule add ad-blackoil ad-core ad-props dual-porosity


Set up grid
^^^^^^^^^^^

.. code-block:: matlab

  G = cartGrid([100, 1, 1], [60, 1, 1]*meter);
  G = computeGeometry(G);


Set up rock properties
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  rock_matrix = makeRock(G, 1*darcy, .3);
  rock_fracture = makeRock(G, 1000*darcy, .01);


Set up fluid
^^^^^^^^^^^^

.. code-block:: matlab

  fluid_matrix = initSimpleADIFluid('phases', 'W', 'c', 1e-06);
  fluid_fracture = initSimpleADIFluid('phases', 'W', 'c', 1e-06);


Set the DP model. Here, a single-phase model is used. Rock and fluid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
are sent in the constructor as a cell array {fracture,matrix}

.. code-block:: matlab

  model = WaterModelDP(G, {rock_fracture,rock_matrix},...
                          {fluid_fracture,fluid_matrix});


Setting transfer function. This step is important to ensure that fluids
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
will be transferred from fracture to matrix (and vice-versa). There are several options of shape factor (see folder transfer_models/shape_factor_models/) that could be set in this constructor. The second argument is the matrix block size.

.. code-block:: matlab

  model.transfer_model_object = KazemiSinglePhaseTransferFunction('KazemiShapeFactor',...
                                                                  [10,10,10]);


Initializing state
^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  state = initResSol(G, 1000*psia);
  state.wellSol = initWellSolAD([], model, state);
  state.pressure_matrix = state.pressure;


Boundary conditions
^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  bc = pside([], G, 'xmin', 0*psia, 'sat', 1);


Solver
^^^^^^

.. code-block:: matlab

  solver = NonLinearSolver();


Validating model
^^^^^^^^^^^^^^^^

.. code-block:: matlab

  model = model.validateModel();


Figure
^^^^^^

.. code-block:: matlab

  fig1 = figure('Position',[100,100,1200,600]);
  fig1.Color = 'w';
  colormap('jet');

.. figure:: mrst/dual_porosity/examples/html/example_1ph_01.png
  :figwidth: 100%


Time loop
^^^^^^^^^

.. code-block:: matlab

  dt = 0.1*day;
  tmax = 100*dt;
  t = 0;
  while t<=tmax
  
      disp(['Time = ',num2str(t/day), ' days'])
      state = solver.solveTimestep(state, dt, model, 'bc', bc);
  
      figure(fig1)
      subplot(2,2,1);
      p = plotCellData(G,state.pressure_matrix/psia);
      p.EdgeAlpha = 0;
      colorbar;
      caxis([0,1000]);
      set(gca,'FontSize',16);
      xlabel('x')
      ylabel('y')
  
      subplot(2,2,3);
      p = plotCellData(G,state.pressure/psia);
      p.EdgeAlpha = 0;
      colorbar;
      caxis([0,1000]);
      set(gca,'FontSize',16);
      xlabel('x')
      ylabel('y')
  
      figure(fig1)
      subplot(2,2,2);
      plot(G.cells.centroids(:,1),state.pressure_matrix/psia,'LineWidth',1.5,'Color','r');
      set(gca,'FontSize',16);
      xlabel('x')
      ylim([0,1000])
      ylabel('Pm [psia]')
  
      subplot(2,2,4);
      plot(G.cells.centroids(:,1),state.pressure/psia,'LineWidth',1.5);
      set(gca,'FontSize',16);
      xlabel('x')
      ylim([0,1000])
      ylabel('Pf [psia]')
  
      drawnow;
  
      t = t+dt;
  
  end


.. code-block:: none

  Time = 0 days
  Time = 0.1 days
  Time = 0.2 days
  Time = 0.3 days
  Time = 0.4 days
  Time = 0.5 days
  Time = 0.6 days
  Time = 0.7 days
  ...

.. figure:: mrst/dual_porosity/examples/html/example_1ph_02.png
  :figwidth: 100%



Two-phase gas injection in a water-saturated dual-porosity reservoir.
---------------------------------------------------------------------
*Generated from example_2ph_drainage.m*

Gas flows in the fracture system, and transfer to the matrix happens via gravity drainage.

.. code-block:: matlab

  clear;
  clc;
  close all;
  
  mrstModule add ad-blackoil ad-core ad-props dual-porosity


Set up grid
^^^^^^^^^^^

.. code-block:: matlab

  G = cartGrid([60, 1, 10], [100, 1, 100]*meter);
  G = computeGeometry(G);


Set up rock properties
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  rock_matrix = makeRock(G, 10*milli*darcy, .1);
  rock_fracture = makeRock(G, 100*milli*darcy, .01);


Set up fluid
^^^^^^^^^^^^

.. code-block:: matlab

  fluid_matrix = initSimpleADIFluid('phases', 'WO', 'c', [1e-12;1e-12],...
                                    'rho',[1000,1]);
  fluid_fracture = initSimpleADIFluid('phases', 'WO', 'c', [1e-12;1e-12],...
                                    'rho',[1000,1]);


Set the DP model. Here, a two-phase model is used. Rock and fluid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
are sent in the constructor as a cell array {fracture,matrix}. We use and OilWater model, where oil plays the role of the gas.

.. code-block:: matlab

  gravity on;
  model = TwoPhaseOilWaterModelDP(G, {rock_fracture,rock_matrix},...
                                        {fluid_fracture,fluid_matrix});


Setting transfer function. This step is important to ensure that fluids
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
will be transferred from fracture to matrix (and vice-versa). There are several options of shape factor (see folder transfer_models/shape_factor_models/) that could be set in this constructor. The second argument is the matrix block size.

.. code-block:: matlab

  model.transfer_model_object = EclipseTwoPhaseTransferFunction('CoatsShapeFactor',[1,1,10]);


Initializing state
^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  state = initResSol(G, 0*psia, [1,0]);
  state.wellSol = initWellSolAD([], model, state);
  state.pressure_matrix = state.pressure;
  state.sm = state.s;


Boundary conditions
^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  bc = pside([], G, 'xmax', 0*psia, 'sat', [0,1]);
  bc = pside(bc, G, 'xmin', 500*psia, 'sat', [0,1]);


Solver
^^^^^^

.. code-block:: matlab

  solver = NonLinearSolver();


Validating model
^^^^^^^^^^^^^^^^

.. code-block:: matlab

  model = model.validateModel();


Figure
^^^^^^

.. code-block:: matlab

  fig1 = figure('Position',[100,100,1200,600]);
  fig1.Color = 'w';
  colormap('jet');

.. figure:: mrst/dual_porosity/examples/html/example_2ph_drainage_01.png
  :figwidth: 100%


Time loop
^^^^^^^^^

.. code-block:: matlab

  dt = 5*day;
  tmax = 100*dt;
  t = 0;
  while t<=tmax
  
      disp(['Time = ',num2str(t/day), ' days'])
      state = solver.solveTimestep(state, dt, model, 'bc', bc);
  
      figure(fig1)
      subplot(2,1,1);
      p = plotCellData(G,state.s(:,1));
      p.EdgeAlpha = 0;
      view(0,0);
      colorbar;
      caxis([0,1]);
      set(gca,'FontSize',16);
      xlabel('x')
      zlabel('z')
      title('Fractures');
  
      subplot(2,1,2);
      p = plotCellData(G,state.sm(:,1));
      p.EdgeAlpha = 0;
      view(0,0);
      colorbar;
      caxis([0,1]);
      set(gca,'FontSize',16);
      xlabel('x')
      zlabel('z')
      title('Matrix');
  
      drawnow;
  
      t = t+dt;
  
  end


.. code-block:: none

  Time = 0 days
  Time = 5 days
  Time = 10 days
  Time = 15 days
  Time = 20 days
  Time = 25 days
  Time = 30 days
  Time = 35 days
  ...

.. figure:: mrst/dual_porosity/examples/html/example_2ph_drainage_02.png
  :figwidth: 100%



Two-phase water injection in an oil-saturated dual-porosity reservoir.
----------------------------------------------------------------------
*Generated from example_2ph_imbibition.m*

Water flows quickly in the fracture system, while transfer to the matrix happens via spontaneous imbibition.

.. code-block:: matlab

  clear;
  clc;
  close all;
  
  mrstModule add ad-blackoil ad-core ad-props dual-porosity


Set up grid
^^^^^^^^^^^

.. code-block:: matlab

  G = cartGrid([60, 1, 1], [60, 1, 1]*meter);
  G = computeGeometry(G);


Set up rock properties
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  rock_matrix = makeRock(G, 10*milli*darcy, .1);
  rock_fracture = makeRock(G, 100*milli*darcy, .01);


Set up fluid
^^^^^^^^^^^^

.. code-block:: matlab

  fluid_matrix = initSimpleADIFluid('phases', 'WO', 'c', [1e-12;1e-12]);
  fluid_fracture = initSimpleADIFluid('phases', 'WO', 'c', [1e-12;1e-12]);
  Pe = 100*psia;
  fluid_matrix.pcOW = @(sw)Pe;


Set the DP model. Here, a two-phase model is used. Rock and fluid
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
are sent in the constructor as a cell array {fracture,matrix}

.. code-block:: matlab

  model = TwoPhaseOilWaterModelDP(G, {rock_fracture,rock_matrix},...
                                     {fluid_fracture,fluid_matrix});


Setting transfer function. This step is important to ensure that fluids
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
will be transferred from fracture to matrix (and vice-versa). There are several options of shape factor (see folder transfer_models/shape_factor_models/) that could be set in this constructor. The second argument is the matrix block size. Another possible transfer function to be used in this model would be:       model.transfer_model_object = EclipseTransferFunction();

.. code-block:: matlab

  model.transfer_model_object = KazemiTwoPhaseTransferFunction('KazemiShapeFactor',...
                                                                [5,5,5]);


Initializing state
^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  state = initResSol(G, 0*psia, [0,1]);
  state.wellSol = initWellSolAD([], model, state);
  state.pressure_matrix = state.pressure;
  state.sm = state.s;


Boundary conditions
^^^^^^^^^^^^^^^^^^^

.. code-block:: matlab

  bc = pside([], G, 'xmax', 0*psia, 'sat', [1,0]);
  bc = pside(bc, G, 'xmin', 1000*psia, 'sat', [1,0]);


Solver
^^^^^^

.. code-block:: matlab

  solver = NonLinearSolver();


Validating model
^^^^^^^^^^^^^^^^

.. code-block:: matlab

  model = model.validateModel();


Figure
^^^^^^

.. code-block:: matlab

  fig1 = figure('Position',[100,100,1200,600]);
  fig1.Color = 'w';
  colormap('jet');

.. figure:: mrst/dual_porosity/examples/html/example_2ph_imbibition_01.png
  :figwidth: 100%


Time loop
^^^^^^^^^

.. code-block:: matlab

  dt = 5*day;
  tmax = 100*dt;
  t = 0;
  while t<=tmax
  
      disp(['Time = ',num2str(t/day), ' days'])
      state = solver.solveTimestep(state, dt, model, 'bc', bc);
  
      figure(fig1)
      subplot(2,2,1);
      p = plotCellData(G,state.s(:,1));
      p.EdgeAlpha = 0;
      colorbar;
      caxis([0,1]);
      set(gca,'FontSize',16);
      xlabel('x')
      ylabel('y')
  
      subplot(2,2,3);
      p = plotCellData(G,state.sm(:,1));
      p.EdgeAlpha = 0;
      colorbar;
      caxis([0,1]);
      set(gca,'FontSize',16);
      xlabel('x')
      ylabel('y')
  
      figure(fig1)
      subplot(2,2,2);
      plot(G.cells.centroids(:,1),state.s(:,1),'LineWidth',1.5,'Color','r');
      set(gca,'FontSize',16);
      xlabel('x')
      ylim([0,1])
      ylabel('Swf [-]')
  
      subplot(2,2,4);
      plot(G.cells.centroids(:,1),state.sm(:,1),'LineWidth',1.5);
      set(gca,'FontSize',16);
      xlabel('x')
      ylim([0,1])
      ylabel('Swm [-]')
  
      drawnow;
  
      t = t+dt;
  
  end


.. code-block:: none

  Time = 0 days
  Time = 5 days
  Time = 10 days
  Time = 15 days
  Time = 20 days
  Time = 25 days
  Time = 30 days
  Time = 35 days
  ...

.. figure:: mrst/dual_porosity/examples/html/example_2ph_imbibition_02.png
  :figwidth: 100%


